<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <title>Admin</title>
    <meta charset="UTF-8">
  <title>Responsive Side Navigation Bar</title>
  <link rel="stylesheet" href="styles.css">
  <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script>
    $(document).ready(function(){
      $(".hamburger").click(function(){
         $(".wrapper").toggleClass("collapse");
      });
    });
  </script>
    <style type="text/css">
      
      @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap');
        *{
          margin: 0;
          padding: 0;
          list-style: none;
          text-decoration: none;
          box-sizing: border-box;
          font-family: 'Montserrat', sans-serif;
        }
        body{
          background: #e1ecf2;
        }
        .wrapper{
          margin: 10px;
        }
        .wrapper .top_navbar{
          width: calc(100% - 20px);
          height: 60px;
          display: flex;
          position: fixed;
          top: 10px;
        }

        .wrapper .top_navbar .hamburger{
          width: 70px;
          height: 100%;
          background: #204051;
          padding: 15px 17px;
          border-top-left-radius: 20px;
          cursor: pointer;
        }

        .wrapper .top_navbar .hamburger div{
          width: 35px;
          height: 4px;
          background: #92a6e2;
          margin: 5px 0;
          border-radius: 5px;
        }

        .wrapper .top_navbar .top_menu{
          width: calc(100% - 70px);
          height: 100%;
          background: #fff;
          border-top-right-radius: 20px;
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 0 20px;
          box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        }

        .wrapper .top_navbar .top_menu .logo{
          color: #2e4ead;
          font-size: 20px;
          font-weight: 700;
          letter-spacing: 3px;
        }

        .wrapper .top_navbar .top_menu ul{
          display: flex;
        }

        .wrapper .top_navbar .top_menu ul li a{
          display: block;
          margin: 0 10px;
          width: 35px;
          height: 35px;
          line-height: 35px;
          text-align: center;
          border: 1px solid #2e4ead;
          border-radius: 50%;
          color: #2e4ead;
        }

        .wrapper .top_navbar .top_menu ul li a:hover{
          background: #4360b5;
          color: #fff;
        }

        .wrapper .top_navbar .top_menu ul li a:hover i{
          color: #fff;
        }

        .wrapper .sidebar{
          position: fixed;
          top: 70px;
          left: 10px;
          background: #204051;
          width: 250px;
          height: calc(100% - 80px);
          border-bottom-left-radius: 20px;
          transition: all 0.3s ease;
        }

        .wrapper .sidebar ul li a{
            display: block;
            padding: 16px;
            color: #fff;
            position: relative;
            margin-bottom: 1px;
            color: #92a6e2;
            white-space: nowrap;
        }

        .wrapper .sidebar ul li a:before{
          content: "";
          position: absolute;
          top: 0;
          left: 0;
          width: 3px;
          height: 100%;
          background: #92a6e2;
          display: none;
        }

        .wrapper .sidebar ul li a span.icon{
          margin-right: 10px;
          display: inline-block;
        }

        .wrapper .sidebar ul li a span.title{
          display: inline-block;
        }

        .wrapper .sidebar ul li a:hover,
        .wrapper .sidebar ul li a.active{
          background: #4360b5;
          color: #fff;
        }

        .wrapper .sidebar ul li a:hover:before,
        .wrapper .sidebar ul li a.active:before{
          display: block;
        }

        .wrapper .main_container{
          width: (100% - 200px);
          margin-top: 70px;
          margin-left: 250px;
          padding: 15px;
          transition: all 0.3s ease;
        }

        .wrapper .main_container .item{
          background: #fff;
          margin-bottom: 10px;
          padding: 15px;
          font-size: 14px;
          line-height: 22px;
        }

        .wrapper.collapse .sidebar{
          width: 70px;
        }

        .wrapper.collapse .sidebar ul li a{
          text-align: center; 
        }

        .wrapper.collapse .sidebar ul li a span.icon{
          margin: 0;
        }

        .wrapper.collapse .sidebar ul li a span.title{
          display: none;
        }

        .wrapper.collapse .main_container{
          width: (100% - 70px);
          margin-left: 70px;
        }
        i:hover{
          color: red;
        }
    </style>
  </head>
  <body>

<div class="wrapper">
  <div class="top_navbar">
    <div class="hamburger">
       <div class="one"></div>
       <div class="two"></div>
       <div class="three"></div>
    </div>
    <div class="top_menu">
      {{-- on logo click, you will navigate to same page --}}
      <div class="logo"><a href="{{ url('home') }}"><img src="{{ asset('images/logo.png') }}" width="120px"></a></div>

 <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt" style="font-size: 25px"></i>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
      
    </div>
  </div>
  
  <div class="sidebar">
      <br>
      <ul>
        <li><a href="{{ url('home') }}">
          <span class="icon"><i class="fa fa-cube" style="color: white"></i></span>
          <span class="title">Dashboard</span></a></li>
        <li><a href="{{ url('fetchtestimonial')}}">
          <span class="icon"><i class="fa fa-map-marker"></i></span>
          <span class="title">Testimonial</span></a></li>  
          <li><a href="{{ url('fetchdestination')}}">
            <span class="icon"><i class="fa fa-map-marker"></i></span>
            <span class="title">Hotels </span></a></li>
          <li><a href="{{ url('fetchTeam') }}">
            <span class="icon"><i class="fas fa-file-video"></i></span>
            <span class="title">Team</span>
            </a></li>
          <li><a href="{{ url('fetchMode') }}">
            <span class="icon"><i class="fa fa-plus"></i></span>
            <span class="title">Transportaion</span>
            </a></li>
          <li><a href="{{ url('fetchpost') }}">
           <span class="icon"><i class="fa fa-plus"></i></span>
              <span class="title">Payment</span>
              </a></li>
              <li><a href="{{ url('fetchFlight') }}">
                <span class="icon"><i class="fa fa-plus"></i></span>
                <span class="title">Fligth</span>
                </a></li>
                <li><a href="{{ url('fetchStour') }}">
                  <span class="icon"><i class="fa fa-plus"></i></span>
                  <span class="title">Special Tours</span>
                  </a></li>
              <li><a href="{{ url('fetchTour') }}">
                <span class="icon"><i class="fa fa-envelope"></i></span>
                <span class="title">Booking</span>
                </a></li>     
                <li><a href="{{ url('fetchcontact') }}">
                  <span class="icon"><i class="fa fa-envelope"></i></span>
                  <span class="title">Contact</span>
                  </a></li>   
        
    </ul>
  </div>
  
  <div class="main_container">
     @yield('content')   
  </div>
</div>  

  </body>
</html>