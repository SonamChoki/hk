@extends('layouts.base')
<style type="text/css">
  .site-footer
{
  background-color:#26272b;
  padding:45px 0 20px;
  font-size:15px;
  line-height:24px;
  color:#737373;
}
.site-footer hr
{
  border-top-color:#bbb;
  opacity:0.5
}
.site-footer hr.small
{
  margin:20px 0
}
.site-footer h6
{
  color:#fff;
  font-size:16px;
  text-transform:uppercase;
  margin-top:5px;
  letter-spacing:2px
}
.site-footer a
{
  color:#737373;
}
.site-footer a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links
{
  padding-left:0;
  list-style:none
}
.footer-links li
{
  display:block
}
.footer-links a
{
  color:#737373
}
.footer-links a:active,.footer-links a:focus,.footer-links a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links.inline li
{
  display:inline-block
}
.site-footer .social-icons
{
  text-align:right
}
.site-footer .social-icons a
{
  width:40px;
  height:40px;
  line-height:40px;
  margin-left:6px;
  margin-right:0;
  border-radius:100%;
  background-color:#33353d
}
.copyright-text
{
  margin:0
}
@media (max-width:991px)
{
  .site-footer [class^=col-]
  {
    margin-bottom:30px
  }
}
@media (max-width:767px)
{
  .site-footer
  {
    padding-bottom:0
  }
  .site-footer .copyright-text,.site-footer .social-icons
  {
    text-align:center
  }
}
.social-icons
{
  padding-left:0;
  margin-bottom:0;
  list-style:none
}
.social-icons li
{
  display:inline-block;
  margin-bottom:4px
}
.social-icons li.title
{
  margin-right:15px;
  text-transform:uppercase;
  color:#96a2b2;
  font-weight:700;
  font-size:13px
}
.social-icons a{
  background-color:#eceeef;
  color:#818a91;
  font-size:16px;
  display:inline-block;
  line-height:44px;
  width:44px;
  height:44px;
  text-align:center;
  margin-right:8px;
  border-radius:100%;
  -webkit-transition:all .2s linear;
  -o-transition:all .2s linear;
  transition:all .2s linear
}
.social-icons a:active,.social-icons a:focus,.social-icons a:hover
{
  color:#fff;
  background-color:#29aafe
}
.social-icons.size-sm a
{
  line-height:34px;
  height:34px;
  width:34px;
  font-size:14px
}
.social-icons a.facebook:hover
{
  background-color:#3b5998
}
.social-icons a.twitter:hover
{
  background-color:#00aced
}
.social-icons a.linkedin:hover
{
  background-color:#007bb6
}
.social-icons a.dribbble:hover
{
  background-color:#ea4c89
}
@media (max-width:767px)
{
  .social-icons li.title
  {
    display:block;
    margin-right:0;
    font-weight:600
  }
}
#popup-img{
 border: 3px solid #fff; 
}
</style>
@section('content')
<br><br>

 @foreach($posts as $post)
  <div class="row" style="padding: 15px; background-color: white">
    <br>
    <div class="col">
      <center><b><h5>{{ $post->title }}</h5> <small>Posted on : {{ $post->created_at }}</small></b></center><hr width="50%">
      <div class="row">
        <div class="col-md-4">
          <img class="img-fluid popup" src="{{ asset('uploads/post/' . $post->image) }}" style="cursor: pointer;" id="picture()" width="100%">
        </div>
        <div class="col-md-8">
          <br><br>
          <p>{{ $post->descriptions }}</p>
        </div>
      </div>
      <br>
    </div>
  </div><br>
  @endforeach



  <!--footer-->
   <div class="row" style="background-color: white; padding: 15px;">
      <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About Nepal Travels and Tours</h6>
            <p class="text-justify">100 Jahre sind es her, seit Toni Hagen in Luzern das Licht der Welt erblickte. Im fernen Nepal kennt ihn jeder, in seiner Heimat muss er in Erinnerung gerufen werden. Toni Hagen, der Ingenieur-Geologe, wurde ein bedeutender, aber nonkonformer Entwicklungshelfer, der zu Lebzeiten im eigenen Land verkannt war und erst nach seinem Tod rehabilitiert wurde.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Contact Information</h6>
            <ul class="footer-links">
              <i class="fa fa-map-marker" aria-hidden="true"></i> Austrasse 20, Ch-4051 Basel / Switzerland <br>
             <i class="fa fa-phone" aria-hidden="true"></i> 0041 78 800 21 34 <br>
             <i class="fa fa-envelope" aria-hidden="true" ></i><a href="mailto:peterlangendorf@gmx.com"> peterlangendorf@gmx.com</a>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
            <a href="{{ url('about') }}">about</a><br>
            <a href="{{ url('showdestination') }}">our destination</a><br>
            <a href="{{ url('showoffer') }}">offers</a><br>
            <a href="{{ url('showgallery') }}">gallery</a><br>
            <a href="{{ url('contactform') }}">contact</a>

            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2020 | All rights reserved | Web designed by 
         <a href="webdynobhutan.com" style="color: teal">webdyno-bhutan</a>.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="linkedin" href="https://www.linkedin.com/in/peter-c-langendorf-86590548/"><i class="fa fa-linkedin"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
</footer>
        </div>
@endsection