@extends('layouts.admin')
<style type="text/css">
	table { 
  width: 100%; 
  border-collapse: collapse; 
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #eee; 
}
th { 
  background: #3b6978; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 16px; 
  border: 1px solid #ccc; 
  text-align: justify; 
}
a:hover{

}
</style>
@section('content')
<div class="container" style="background-color: white; padding: 15px;">
	<a href="{{ url('home') }}">Dashboard</a> | View Mode <br><br><hr style="background-color: #679b9b"><br><br>
	<a href="{{ url('mode') }}" style="padding: 10px;background-color: #3ca59d;border-radius: 15px;color: white">Add Mode</a>
	<br><br>
</div>
<br>
<div class="container" style="background-color: white; padding: 15px">
	<table>
	<thead>
	<tr>
		<th>Transportation</th>
		<th>Description</th>
		<th>Image</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
	</thead>
	<tbody>
		@foreach($modes as $mode)
	<tr>
		<td>{{ $mode->title }}</td>
		<td>{{ $mode->describtions }}</td>
		<td><center><img src="{{ asset('uploads/mode/' . $mode->image) }}" width="280px" height="250px" style="border-radius: 12px;object-fit: cover;"></center></td>
		<td><a href="/editMode/{{ $mode->id }}" style="color: #4f8a8b">Edit</a></td>
		<td><a href="/deleteMode/{{ $mode->id }}" style="color: #e84a5f">Delete</a></td>
		
	</tr>
	@endforeach
	</tbody>
</table>
</div><br>
<div class="container" style="background-color: white;padding: 15px">
  <center>© 2021 | Happiness Kingdom </center>
</div>
@endsection
