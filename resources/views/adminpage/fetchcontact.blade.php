@extends('layouts.admin')
<style type="text/css">
	table { 
  width: 100%; 
  border-collapse: collapse; 
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #eee; 
}
th { 
  background: #3b6978; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 16px; 
  border: 1px solid #ccc; 
  text-align: justify; 
}
a:hover{

}
</style>
@section('content')
<div class="container" style="background-color: white; padding: 15px;">
	<a href="{{ url('home') }}">Dashboard</a> | Contacts <br><br><hr style="background-color: #ebebeb">
	<br><br>
</div>
<br>
<div class="container" style="background-color: white; padding: 15px">
	<table>
	<thead>
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Message</th>
		<th>Delete</th>
	</tr>
	</thead>
	<tbody>
		@foreach($posts as $post)
	<tr>
		<td>{{ $post->name }}</td>
		<td>{{ $post->email }}</td>
		<td>{{ $post->message }}</td>
		<td><a href="/deletecontact/{{ $post->id }}" style="color: #e84a5f">Delete</a></td>
	</tr>
	@endforeach
	</tbody>
</table>
</div><br>
<div class="container" style="background-color: white;padding: 15px">
  <center>© 2021 | Happiness Kingdom </center>
  {{-- mergetrial --}}
</div>
@endsection
