@extends('layouts.admin')
<style type="text/css">
	table { 
  width: 100%; 
  border-collapse: collapse; 
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #eee; 
}
th { 
  background: #3b6978; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 16px; 
  border: 1px solid #ccc; 
  text-align: justify; 
}
a:hover{

}
</style>
@section('content')
<div class="container" style="background-color: white; padding: 15px;">
	<a href="{{ url('home') }}">Dashboard</a> | View Tour <br><br><hr style="background-color: #679b9b"><br><br>
	<a href="{{ url('tour') }}" style="padding: 10px;background-color: #3ca59d;border-radius: 15px;color: white">Add Tours</a>
	<br><br>
</div>
<br>
<div class="container" style="background-color: white; padding: 15px">
	<table>
	<thead>
	<tr>
		<th>Title</th>
		<th>Description</th>
		<th>image</th>
		<th>price</th>
		<th>Edit</th>
		<th>Delete</th>
		<th>Customer</th>

	

	</tr>
	</thead>
	<tbody>
		@foreach($tours as $tour)
	<tr>
		<td>{{ $tour->title }}</td>
		<td>{{ $tour->descriptions }}</td>
		<td><center><img src="{{ asset('uploads/tour/' . $tour->image) }}" width="100px" height="150px" style="border-radius: 12px;object-fit: cover;"></center></td>
		<td>{{ $tour->price }}</td>
		<td><a href="/editTour/{{ $tour->id }}" style="color: #4f8a8b">Edit</a></td>
		<td><a href="/deleteTour/{{ $tour->id }}" style="color: #e84a5f">Delete</a></td>
		<td><a href="/editTour/{{ $tour->id }}" style="color: #4f8a8b">Customers</a></td>

		
	</tr>
	@endforeach
	</tbody>
</table>
</div><br>
<div class="container" style="background-color: white;padding: 15px">
  <center>© 2021 | Happiness Kingdom </center>
</div>
@endsection
