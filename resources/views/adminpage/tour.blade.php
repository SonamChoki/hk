@extends('layouts.admin')
<style>
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
@section('content')
<div class="container" style="background-color: white">
	<a href="{{ url('home') }}">Dashboard</a> | Add Tour <br><br><hr style="background-color: #679b9b"><br><br>
	<a href="{{ url('fetchTour') }}" style="padding: 10px;background-color: #3ca59d;border-radius: 15px;color: white">View Tour</a><br><br>
</div>
<br>
<div class="container">
	<form action="{{ route('createtour') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
	  <label>Upcoming Tour</label><br>
	  <input type="text" id="title" name="title" required="title"><br>
	  <label>Descriptions</label><br>
	  <textarea id="descriptions" name="descriptions" style="height: 150px" required="descriptions"> </textarea> 
    <label>Price</label><br>
	  <input type="text" id="price" name="price" required="price"><br>
    <label>Image Upload</label><br>
	  <input type="file" name="image" id="image" required="image"><br><br>
	  <input type="submit" name="upload">
	  <br><br>
	</form>
</div>
<br>
<div class="container" style="background-color: white">
  <center>© 2021 | Happiness Kingdom </center>
</div>
@endsection