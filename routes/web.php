<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/', 'welcome');

//User authentication
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact', 'ContactController@index')->name('contact');

//Testimonial
Route::get('testimonial','TestimonialController@index')->name('testimonial');
Route::get('/fetchtestimonial','TestimonialController@display')->name('fetchtestimonial');
Route::post('/createtestimonial','TestimonialController@store')->name('createtestimonial');
Route::get('/edittestimonial/{id}','TestimonialController@edit')->name('edittestimonial');
Route::put('/updatetestimonial/{id}','TestimonialController@update')->name('updatetestimonial');
Route::get('/deletetestimonial/{id}','TestimonialController@delete')->name('deletetestimonial');

//Contacts
Route::get('contactform','ContactController@index')->name('contactform');
Route::post('/createcontact','ContactController@store')->name('createcontact');
Route::get('/fetchcontact','ContactController@display')->name('fetchcontact');
Route::get('/deletecontact/{id}','ContactController@delete')->name('deletecontact');

//Team
Route::get('/fetchTeam','TeamController@display')->name('fetchpost');
Route::get('team','TeamController@index')->name('post');
Route::post('/createTeam','TeamController@store')->name('createpost');
Route::get('/editTeam/{id}','TeamController@edit')->name('editpost');
Route::put('/updateTeam/{id}','TeamController@update')->name('updatepost');
Route::get('/deleteTeam/{id}','TeamController@delete')->name('deletepost');

//Mode of Transportation
Route::get('/fetchMode','ModeController@display')->name('fetchmode');
Route::get('mode','ModeController@index')->name('mode');
Route::post('/createMode','ModeController@store')->name('createmode');
Route::get('/editMode/{id}','ModeController@edit')->name('editmode');
Route::put('/updateMode/{id}','ModeController@update')->name('updatemode');
Route::get('/deleteMode/{id}','ModeController@delete')->name('deletemode');

//Upcoming Tours
Route::get('/fetchTour','TourController@display')->name('fetchtour');
Route::get('tour','TourController@index')->name('tour');
Route::post('/createTour','TourController@store')->name('createtour');
Route::get('/editTour/{id}','TourController@edit')->name('edittour');
Route::put('/updateTour/{id}','TourController@update')->name('updatetour');
Route::get('/deleteTour/{id}','TourController@delete')->name('deletetour');

//Special Tours
Route::get('/fetchStour','StourController@display')->name('fetchstour');
Route::get('stour','StourController@index')->name('stour');
Route::post('/createStour','StourController@store')->name('createstour');
Route::get('/editStour/{id}','StourController@edit')->name('editstour');
Route::put('/updateStour/{id}','StourController@update')->name('updatestour');
Route::get('/deleteStour/{id}','StourController@delete')->name('deletestour');









