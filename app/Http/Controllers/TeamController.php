<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class TeamController extends Controller
{
     public function index()
     {
     	return view('adminpage.post');
     }
     public function store(Request $request)
     {
     	$post = new Post();
     	$post->title = $request->input('title');
     	$post->descriptions = $request->input('descriptions');     	

     	if ($request->hasfile('image')) {
     		$file=$request->file('image');
     		$extension=$file->getClientOriginalExtension();
     		$filename=time() . '.' . $extension;
     		$file->move('uploads/post/',$filename);
     		$post->image=$filename;
     	}
     	else {
     		return $request;
     		$post->image='';
     	}

     	$post->save();
     	echo '<script>alert("Posted successfully!")</script>'; 
     	return view('adminpage.post')->with('post',$post);
     }
     public function display()
     {
     	$posts = Post::all();
     	return view('adminpage.fetchpost')->with('posts',$posts);
     }
     public function edit($id)
     {
     	$posts = Post::find($id);
     	return view('adminpage.editpost')->with('posts',$posts);
     }
     public function showblog()
     {
          $posts = Post::all();
          return view('showblog')->with('posts',$posts);
     }
     public function update(Request $request,$id)
     {
     	$post = Post::find($id);
     	$post->title = $request->input('title');
     	$post->descriptions = $request->input('descriptions');
     	

     	if ($request->hasfile('image')) {
     		$file=$request->file('image');
     		$extension=$file->getClientOriginalExtension();
     		$filename=time() . '.' . $extension;
     		$file->move('uploads/post/',$filename);
     		$post->image=$filename;
     	}

     	$post->save();
     	echo '<script>alert("updated successfully!")</script>'; 
     	return redirect('fetchTeam')->with('post',$post);
     }
     public function delete($id)
     {
     	$post = Post::find($id);
     	$post->delete();
     	return redirect('fetchTeam')->with('post',$post);
     }
}
