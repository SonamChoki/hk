<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonial;

class TestimonialController extends Controller
{
    public function index()
     {
     	return view('adminpage.testimonial');
     }
     public function store(Request $request)
     {
     	
          $testimonial = new Testimonial();
     	$testimonial->name = $request->input('name');
     	$testimonial->testimonial = $request->input('testimonial');
          
     	$testimonial->save();
     	echo '<script>alert("Testimonial successfully Posted!")</script>';
     	return redirect('fetchtestimonial')->with('testimonial',$testimonial); 
     }
     public function display()
     {
     	$testimonials = Testimonial::all();
     	return view('adminpage.fetchtestimonial')->with('testimonials',$testimonials);
     }
     public function edit($id)
     {
     	$testimonial = Testimonial::find($id);
     	return view('adminpage.edittestimonial')->with('testimonial',$testimonial);
     }
     public function showblog()
     {
          $testimonials= Testimonial::all();
          return view('showtestimonial')->with('posts',$testimonials);
     }
     public function update(Request $request,$id)
     {
     	$testimonial = Testimonial::find($id);
     	$testimonial->name = $request->input('name');
     	$testimonial->testimonial = $request->input('testimonial');
    

     	$testimonial->save();
     	echo '<script>alert("updated successfully!")</script>'; 
     	return redirect('fetchtestimonial')->with('testimonial',$testimonial);
     }
     public function delete($id)
     {
     	$testimonial = Testimonial::find($id);
     	$testimonial->delete();
     	return redirect('fetchtestimonial')->with('testimonial',$testimonial);
     }
}
