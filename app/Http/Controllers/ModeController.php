<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mode;
class ModeController extends Controller
{


    public function index()
    {
        return view('adminpage.mode');
    }

    public function display()
    {
        $modes = Mode::all();
        return view('adminpage.fetchmode')->with('modes',$modes);
    }
    public function store(Request $request)
    {
        $mode = new Mode();
        $mode->title = $request->input('title');
        $mode->descriptions = $request->input('descriptions');     	

        if ($request->hasfile('image')) {
            $file=$request->file('image');
            $extension=$file->getClientOriginalExtension();
            $filename=time() . '.' . $extension;
            $file->move('uploads/mode/',$filename);
            $mode->image=$filename;
        }
        else {
            return $request;
            $mode->image='';
        }

        $mode->save();
        echo '<script>alert("Posted successfully!")</script>'; 
        return view('adminpage.mode')->with('mode',$mode);
    }
    public function edit($id)
    {
        $modes = Mode::find($id);
        return view('adminpage.editmode')->with('modes',$modes);
    }
    public function update(Request $request,$id)
    {
        $mode = Mode::find($id);
        $mode->title = $request->input('title');
        $mode->descriptions = $request->input('descriptions');
        

        if ($request->hasfile('image')) {
            $file=$request->file('image');
            $extension=$file->getClientOriginalExtension();
            $filename=time() . '.' . $extension;
            $file->move('uploads/mode/',$filename);
            $mode->image=$filename;
        }

        $mode->save();
        echo '<script>alert("updated successfully!")</script>'; 
        return redirect('fetchMode')->with('mode',$mode);
    }
    public function delete($id)
    {
        $mode = Mode::find($id);
        $mode->delete();
        return redirect('fetchMode')->with('mode',$mode);
    }

  



}
