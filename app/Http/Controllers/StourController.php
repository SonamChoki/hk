<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stour;
use App\tour;


class StourController extends Controller
{
     public function index()
     {
     	return view('adminpage.stour');
     }
     public function store(Request $request)
     {
     	$stour = new Stour();
     	$stour->title = $request->input('title');
		 $stour->title = $request->input('title');
     	$stour->descriptions = $request->input('descriptions'); 
    	

     	if ($request->hasfile('image')) {
     		$file=$request->file('image');
     		$extension=$file->getClientOriginalExtension();
     		$filename=time() . '.' . $extension;
     		$file->move('uploads/stour/',$filename);
     		$stour->image=$filename;
     	}

     	else {
     		return $request;
     		$stour->image='';
     	}

     	$stour->save();
     	echo '<script>alert("Posted successfully!")</script>'; 
     	return view('adminpage.stour')->with('stour',$stour);
     }
	 //fetch
     public function display()
     {
     	$stours = Stour::all();
     	return view('adminpage.fetchstour')->with('stours',$stours);
     }
     public function edit($id)
     {
     	$stours = Stour::find($id);
     	return view('adminpage.editstour')->with('stours',$stours);
     }
    
     public function update(Request $request,$id)
     {
     	$stour = Stour::find($id);
     	$stour->title = $request->input('title');
     	$stour->descriptions = $request->input('descriptions');

     	if ($request->hasfile('image')) {
     		$file=$request->file('image');
     		$extension=$file->getClientOriginalExtension();
     		$filename=time() . '.' . $extension;
     		$file->move('uploads/stour/',$filename);
     		$stour->image=$filename;
     	}


     	$stour->save();
     	echo '<script>alert("updated successfully!")</script>'; 
     	return redirect('fetchStour')->with('stour',$stour);
     }
     public function delete($id)
     {
     	$stour = Stour::find($id);
     	$stour->delete();
     	return redirect('fetchStour')->with('stour',$stour);
     }
}
