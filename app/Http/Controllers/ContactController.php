<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
class ContactController extends Controller
{
     public function store(Request $request)
     {
     	$post = new Contact();
     	$post->name = $request->input('name');
     	$post->email = $request->input('email');
     	$post->message = $request->input('message');
     	$post->save();
     	echo '<script>alert(" Submitted successfully!")</script>'; 
     	return view('contactform')->with('post',$post);
     }
     public function display()
     {
     	$posts = Contact::all();
     	return view('adminpage.fetchcontact')->with('posts',$posts);
     }
     public function index()
     {
     	return view('contactform');
     }
     public function delete($id)
     {
     	$post = Contact::find($id);
     	$post->delete();
     	return redirect('fetchcontact')->with('post',$post);
     }
}
