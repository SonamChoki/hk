<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour;
class TourController extends Controller
{
     public function index()
     {
     	return view('adminpage.tour');
     }
     public function store(Request $request)
     {
     	$tour = new Tour();
     	$tour->title = $request->input('title');
		 $tour->title = $request->input('title');
     	$tour->descriptions = $request->input('descriptions'); 
		 $tour->price = $request->input('price');     	
    	

     	if ($request->hasfile('image')) {
     		$file=$request->file('image');
     		$extension=$file->getClientOriginalExtension();
     		$filename=time() . '.' . $extension;
     		$file->move('uploads/tour/',$filename);
     		$tour->image=$filename;
     	}
     	else {
     		return $request;
     		$tour->image='';
     	}

     	$tour->save();
     	echo '<script>alert("Posted successfully!")</script>'; 
     	return view('adminpage.tour')->with('tour',$tour);
     }
	 //fetch
     public function display()
     {
     	$tours = Tour::all();
     	return view('adminpage.fetchtour')->with('tours',$tours);
     }
     public function edit($id)
     {
     	$tours = Tour::find($id);
     	return view('adminpage.edittour')->with('tours',$tours);
     }
    
     public function update(Request $request,$id)
     {
     	$tour = Tour::find($id);
     	$tour->title = $request->input('title');
		 $tour->price = $request->input('price');
     	$tour->descriptions = $request->input('descriptions');
     	

     	if ($request->hasfile('image')) {
     		$file=$request->file('image');
     		$extension=$file->getClientOriginalExtension();
     		$filename=time() . '.' . $extension;
     		$file->move('uploads/tour/',$filename);
     		$tour->image=$filename;
     	}

     	$tour->save();
     	echo '<script>alert("updated successfully!")</script>'; 
     	return redirect('fetchTour')->with('tour',$tour);
     }
     public function delete($id)
     {
     	$tour = Tour::find($id);
     	$tour->delete();
     	return redirect('fetchTour')->with('tour',$tour);
     }
}
