<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
   protected $table='tours'; 
   protected $fillable=['title','descriptions','image','price'];

}

